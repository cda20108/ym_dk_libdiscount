package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Adresse {
	private String numeroRue;
	private String nomRue;
	private String codePostal;
	private String ville;
}
