package fr.afpa.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Users {
	
	
	private String nom;
	private String prenom;
	private String mail;
	private String tel;
	private String librairie;
	private Adresse adresse;
	private Account account;
	
}
