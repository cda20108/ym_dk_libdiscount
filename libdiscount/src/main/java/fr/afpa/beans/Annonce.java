package fr.afpa.beans;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString

public class Annonce {
	
	private int id_annonce;
	private String titre;
	private String Isbn;
	private String maisonEdition;
	private Float prixUnitaire;
	private int remise;
	private int quantite;
	private Float prix_total;
	private LocalDate dateEdition;
	private LocalDate dateAnnonce;
	
}
