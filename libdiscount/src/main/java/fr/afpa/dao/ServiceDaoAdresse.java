package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import fr.afpa.beans.Account;
import fr.afpa.beans.Adresse;
import fr.afpa.beans.Users;

public class ServiceDaoAdresse {

	public void creationAdresseDao(Users users, int id_user) {
		
        
        
        Connection conn = null;
        Statement stm = null;
        
        try {
            
           Class.forName("org.postgresql.Driver");
           
           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
           Properties props = new Properties();
           props.setProperty("user","libdiscount");
           props.setProperty("password","libdiscount");
           
           
           conn =  DriverManager.getConnection(url, props);
           
           System.out.println("Opened database successfully");
           
           
           stm = conn.createStatement();
           
           String query = "insert into adresse( id_adresse, numero_rue, nom_rue, codepostal, id_user, ville)"
                   + "values ( nextval('auto_inc_id_adresse') ,"
                   + "'" + users.getAdresse().getNumeroRue() + "',"
                   + "'" + users.getAdresse().getNomRue() + "',"
                   + "'" + users.getAdresse().getCodePostal() + "',"
                   + "'" + id_user + "',"
                   + "'" + users.getAdresse().getVille()+ "')";
           
           stm.executeUpdate(query);
           
         
           stm.close();
	        conn.close();
           
       } catch (ClassNotFoundException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        catch (SQLException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           }
        

   }

	public Adresse recupAdresse(int id_user) {

		// TODO Auto-generated method stub
		Adresse adresse = new Adresse();
		Connection conn = null;
        Statement stm = null;
        
        
        try {
            
           Class.forName("org.postgresql.Driver");
           
           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
           Properties props = new Properties();
           props.setProperty("user","libdiscount");
           props.setProperty("password","libdiscount");
           
           
           conn =  DriverManager.getConnection(url, props);
           
           System.out.println("Opened database successfully");
           
           
           stm = conn.createStatement();
           
           String query = "select * from adresse where id_user = '"+id_user+"';";
           
        
           
           ResultSet res = stm.executeQuery(query);
           
           String numeroRue;
           String nomRue;
           String codePostal;
           String ville;
        
           while (res.next()) {
               
              
               numeroRue = res.getString("numero_rue");
               nomRue = res.getString("nom_rue");
               codePostal = res.getString("codepostal");
               ville = res.getString("ville");
               
               adresse.setNumeroRue(numeroRue);
               adresse.setNomRue(nomRue);
               adresse.setCodePostal(codePostal);
               adresse.setVille(ville);
               
               
               
           }
           
           
           
           stm.close();
	        conn.close();
           
       } catch (ClassNotFoundException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        catch (SQLException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           }
        
        
		return adresse;
	
	}

	public void modifierAdresse(Users user, int id_user) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "update\r\n"
	           		+ "	       	adresse\r\n"
	           		+ "	       set\r\n"
	           		+ "	       	numero_rue = '"+ user.getAdresse().getNumeroRue() +"',\r\n"
	           		+ "	       	nom_rue = '" + user.getAdresse().getNomRue() + "' ,\r\n"
	           		+ "	       	codepostal = '"+ user.getAdresse().getCodePostal() +"' ,\r\n"
	           		+ "	       	ville = '"+ user.getAdresse().getVille() +"' \r\n"
	           		+ "	       where\r\n"
	           		+ "	       	id_user ="+id_user+";";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
	}

	public void supprimerAdresse(int id_user) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Connection conn = null;
				Statement stm = null;
				try {
					
					 Class.forName("org.postgresql.Driver");
			           
			           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
			           Properties props = new Properties();
			           props.setProperty("user","libdiscount");
			           props.setProperty("password","libdiscount");
			           
			           
			           conn =  DriverManager.getConnection(url, props);
			          
			           System.out.println("Opened database successfully");
			  
			           stm = conn.createStatement();
			           String query = "delete from adresse where id_user = '"+id_user+"';";
			           

			           stm.executeUpdate(query);
			           
			           stm.close();
				       conn.close();
					

				} catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				}
	}
}
