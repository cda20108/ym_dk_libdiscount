
package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;

import fr.afpa.beans.Annonce;
import fr.afpa.beans.Users;

public class ServiceDaoAnnonce {
	
	
	
	
	
	
	
	public ArrayList<Annonce> recupListeAnnonce() {
		ArrayList<Annonce> listeServ = new ArrayList<Annonce>();
		
		 Connection conn = null;
		 Statement stm = null;
		 
		 try {
			 
			Class.forName("org.postgresql.Driver");
			
			String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	        Properties props = new Properties();
	        props.setProperty("user","libdiscount");
	        props.setProperty("password","libdiscount");
			
			
		    conn =  DriverManager.getConnection(url, props);
		    
		    System.out.println("Opened database successfully");
		    
		    
		    stm = conn.createStatement();
		    
		    String query = "select * from annonce";
		    
		    ResultSet listesannonce = stm.executeQuery(query);
		    
		    Annonce annon = null;
		    
		    int id_annonce;
		    String titre;
			String Isbn;
			String maisonEdition;
			Float prixUnitaire;
			int remise;
			int quantite;
			Float prix_total;
			String dateEdition;
			String dateAnnonce;
			
		    
		    while (listesannonce.next()) {
				
		    	id_annonce = listesannonce.getInt("id_annonce");
				titre = listesannonce.getString("titre");
				Isbn = listesannonce.getString("isbn");
				maisonEdition = listesannonce.getString("maison_edition");
				prixUnitaire = listesannonce.getFloat("prix_unitaire");
				remise = listesannonce.getInt("remise");
				quantite = listesannonce.getInt("quantite");
				prix_total = listesannonce.getFloat("prix_total");
				dateEdition= listesannonce.getString("date_edition");
				dateAnnonce = listesannonce.getString("date_annonce");
				
				LocalDate edition = LocalDate.parse(dateEdition);
				LocalDate annonce = LocalDate.parse(dateAnnonce);
				
				annon = new Annonce(id_annonce, titre, Isbn, maisonEdition, prixUnitaire, remise, quantite, prix_total, edition, annonce);
				
				listeServ.add(annon);
				
				  
			}
			
		    stm.close();
		    conn.close(); 
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
		return listeServ;
	}
	

	public void creationAnnonce(Annonce annonce, int id_user) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "insert into annonce( id_annonce, titre, isbn, maison_edition, prix_unitaire, remise, quantite, prix_total, date_edition, date_annonce, id_user)"
	                   + "values ( nextval('auto_inc_id_annonce') ,"
	                   + "'" + annonce.getTitre()+ "'"
	                   + ",'" + annonce.getIsbn()+ "'"
	                   + ",'" + annonce.getMaisonEdition()+ "'"
	                   + ",'" + annonce.getPrixUnitaire()+ "'"
	                   + ",'" + annonce.getRemise()+ "'"
	               	   + ",'" + annonce.getQuantite()+ "'"
	               	   + ",'" + annonce.getPrix_total()+ "'"
	               	   + ",'" + annonce.getDateEdition()+ "'"
	               	   + ",'" + annonce.getDateAnnonce()+ "'"
	                   + ",'" + id_user+"')";
	                  

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
	}
	
	public ArrayList<Annonce> recupListeAnnonceUsers(int id_user) {
		ArrayList<Annonce> listeServ = new ArrayList<Annonce>();
		
		 Connection conn = null;
		 Statement stm = null;
		 
		 try {
			 
			Class.forName("org.postgresql.Driver");
			
			String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	        Properties props = new Properties();
	        props.setProperty("user","libdiscount");
	        props.setProperty("password","libdiscount");
			
			
		    conn =  DriverManager.getConnection(url, props);
		    
		    System.out.println("Opened database successfully");
		    
		    
		    stm = conn.createStatement();
		    
		    String query = "select * from annonce where id_user = '"+id_user+"';";
		    
		    ResultSet listesannonce = stm.executeQuery(query);
		    
		    Annonce annon = null;
		    
		    int id_annonce;
		    String titre;
			String Isbn;
			String maisonEdition;
			Float prixUnitaire;
			int remise;
			int quantite;
			Float prix_total;
			String dateEdition;
			String dateAnnonce;
			
		    
		    while (listesannonce.next()) {
				
		    	id_annonce = listesannonce.getInt("id_annonce");
				titre = listesannonce.getString("titre");
				Isbn = listesannonce.getString("isbn");
				maisonEdition = listesannonce.getString("maison_edition");
				prixUnitaire = listesannonce.getFloat("prix_unitaire");
				remise = listesannonce.getInt("remise");
				quantite = listesannonce.getInt("quantite");
				prix_total = listesannonce.getFloat("prix_total");
				dateEdition= listesannonce.getString("date_edition");
				dateAnnonce = listesannonce.getString("date_annonce");
				
				LocalDate edition = LocalDate.parse(dateEdition);
				LocalDate annonce = LocalDate.parse(dateAnnonce);
				
				annon = new Annonce(id_annonce, titre, Isbn, maisonEdition, prixUnitaire, remise, quantite, prix_total, edition, annonce);
				
				listeServ.add(annon);
				
				 
			}
		    
		    stm.close();
		    conn.close(); 
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
		return listeServ;
	}
	
	public Annonce recupUneAnnonce(int id_user, int id_annonce) {
		Annonce annonce = new Annonce();
		
		 Connection conn = null;
		 Statement stm = null;
		 
		 try {
			 
			Class.forName("org.postgresql.Driver");
			
			String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	        Properties props = new Properties();
	        props.setProperty("user","libdiscount");
	        props.setProperty("password","libdiscount");
			
			
		    conn =  DriverManager.getConnection(url, props);
		    
		    System.out.println("Opened database successfully");
		    
		    
		    stm = conn.createStatement();
		    
		    String query = "select * from annonce where id_user = '"+id_user+"' and id_annonce = '"+id_annonce+"';";
		    
		    ResultSet listesannonce = stm.executeQuery(query);
		    
		    
		    
		    int id_annoncee;
		    String titre;
			String Isbn;
			String maisonEdition;
			Float prixUnitaire;
			int remise;
			int quantite;
			Float prix_total;
			String dateEdition;
			String dateAnnonce;
			
		    
		    while (listesannonce.next()) {
				
		    	id_annoncee = listesannonce.getInt("id_annonce");
				titre = listesannonce.getString("titre");
				Isbn = listesannonce.getString("isbn");
				maisonEdition = listesannonce.getString("maison_edition");
				prixUnitaire = listesannonce.getFloat("prix_unitaire");
				remise = listesannonce.getInt("remise");
				quantite = listesannonce.getInt("quantite");
				prix_total = listesannonce.getFloat("prix_total");
				dateEdition= listesannonce.getString("date_edition");
				dateAnnonce = listesannonce.getString("date_annonce");
				
				LocalDate edition = LocalDate.parse(dateEdition);
				LocalDate dannonce = LocalDate.parse(dateAnnonce);
				
				annonce = new Annonce(id_annoncee, titre, Isbn, maisonEdition, prixUnitaire, remise, quantite, prix_total, edition, dannonce);
				
				
				
				  
			}
		    
		    stm.close();
		    conn.close();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
		return annonce;
	}


	public void modificationAnnonce(Annonce annonce,int id_user,int id_annonce) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "update\r\n"
	           		+ "	       	annonce\r\n"
	           		+ "	       set\r\n"
	           		+ "	       	titre = '"+ annonce.getTitre() +"',\r\n"
	           		+ "	       	isbn = '" + annonce.getIsbn() + "' ,\r\n"
	           		+ "	       	maison_edition = '"+ annonce.getMaisonEdition() +"' ,\r\n"
	           		+ "	       	prix_unitaire = '"+ annonce.getPrixUnitaire() +"' ,\r\n"
	           		+ "	       	remise = '"+ annonce.getRemise() +"',\r\n"
	           		+ "	       	quantite = '"+annonce.getQuantite()+"',\r\n"
	           		+ "	       	prix_total = '"+annonce.getPrix_total()+"',\r\n"
	           		+ "	       	date_edition = '"+ annonce.getDateEdition() +"'\r\n"
	           		+ "	       where\r\n"
	           		+ "	       	id_annonce ="+id_annonce+" and "
	           				+ "id_user = "+id_user+";";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
	}


	public void supprimerAnnonceArchivage(int id_user, int id_annonce) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "delete\r\n"
	           		+ "from\r\n"
	           		+ "	annonce\r\n"
	           		+ "where\r\n"
	           		+ "	id_annonce = '"+id_annonce+"' and id_user = '"+id_user+"';";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
	}


	public void supprimerAnnonce(int id_user) {
				Connection conn = null;
				Statement stm = null;
				try {
					
					 Class.forName("org.postgresql.Driver");
			           
			           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
			           Properties props = new Properties();
			           props.setProperty("user","libdiscount");
			           props.setProperty("password","libdiscount");
			           
			           
			           conn =  DriverManager.getConnection(url, props);
			          
			           System.out.println("Opened database successfully");
			  
			           stm = conn.createStatement();
			           String query = "delete from annonce where id_user = '"+id_user+"';";
			           

			           stm.executeUpdate(query);
			           
			           stm.close();
				       conn.close();
					

			        
				} catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				}
		
	}
	
	
	
}
