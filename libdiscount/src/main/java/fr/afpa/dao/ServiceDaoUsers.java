package fr.afpa.dao;

import java.security.Provider.Service;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Users;

public class ServiceDaoUsers {
	
	public ArrayList<Users> recupListeUsers() {
		
        ArrayList<Users> listeServ = new ArrayList<Users>();
        
         Connection conn = null;
         Statement stm = null;
         
         try {
             
            Class.forName("org.postgresql.Driver");
            
            String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
            Properties props = new Properties();
            props.setProperty("user","libdiscount");
            props.setProperty("password","libdiscount");
            
            
            conn =  DriverManager.getConnection(url, props);
            
            System.out.println("Opened database successfully");
            
            
            stm = conn.createStatement();
            
            String query = "select * from users";
            
            ResultSet listeservice = stm.executeQuery(query);
            
            Users user = null;
            int idUser;
            String nom;
            String prenom;
            String mail;
            String tel;
            String librairie;
            
            while (listeservice.next()) {
                
                idUser = listeservice.getInt(1);
                nom = listeservice.getString("nom");
                prenom = listeservice.getString("prenom");
                mail = listeservice.getString("mail");
                tel = listeservice.getString("tel");
                librairie = listeservice.getString("librairie");
                
                
                user = new Users(nom, prenom, mail, tel, librairie, null, null);
                
                listeServ.add(user);
                
                  
            }
            
            stm.close();
	        conn.close();
            
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
         
        return listeServ;
    }
	
	public void creationUserDao(Users users) {

         Connection conn = null;
         Statement stm = null;
         
         try {
             
            Class.forName("org.postgresql.Driver");
            
            String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
            Properties props = new Properties();
            props.setProperty("user","libdiscount");
            props.setProperty("password","libdiscount");
            
            
            conn =  DriverManager.getConnection(url, props);
            
            System.out.println("Opened database successfully");
            
            
            stm = conn.createStatement();
            
            String query = "insert into users( id_user, Nom, Prenom, Mail, tel, librairie)"
                    + "values ( nextval('auto_inc_users') ,"
                    + "'" + users.getNom() + "',"
                    + "'" + users.getPrenom() + "',"
                    + "'" + users.getMail() + "',"
                    + "'" + users.getTel() + "',"
                    + "'" + users.getLibrairie() + "')";
            
            stm.executeUpdate(query);
            
          
            stm.close();
	        conn.close();
            
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
         

    }
	
	public int recuperationIdUser(String nom, String prenom) {
		
      
         Connection conn = null;
         Statement stm = null;
         int id = 0;
         
         try {
             
            Class.forName("org.postgresql.Driver");
            
            String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
            Properties props = new Properties();
            props.setProperty("user","libdiscount");
            props.setProperty("password","libdiscount");
            
            
            conn =  DriverManager.getConnection(url, props);
            
            System.out.println("Opened database successfully");
            
            
            stm = conn.createStatement();
            
            String query = "select id_user from users where nom = '"+nom+"' and prenom = '"+prenom+"';";
            
            
            
           
            
            ResultSet res = stm.executeQuery(query);
            
            
         
            while (res.next()) {
                
               
                id = res.getInt("id_user");
                
                
            }
            
            //int idServ = listeservice.getInt(1);
            
            stm.close();
	        conn.close();
            
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
         
        return id;
    }

	public Users recupUser(int id_user) {
		// TODO Auto-generated method stub
				Users user = new Users();
				Connection conn = null;
		        Statement stm = null;
		        int id = 0;
		        
		        try {
		            
		           Class.forName("org.postgresql.Driver");
		           
		           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
		           Properties props = new Properties();
		           props.setProperty("user","libdiscount");
		           props.setProperty("password","libdiscount");
		           
		           
		           conn =  DriverManager.getConnection(url, props);
		           
		           System.out.println("Opened database successfully");
		           
		           
		           stm = conn.createStatement();
		           
		           String query = "select * from users where id_user = '"+id_user+"';";
		           
		        
		           
		           ResultSet res = stm.executeQuery(query);
		           
		           String nom = null;
		           String prenom = null;
		           String mail = null;
		           String tel = null;
		           String librairie = null;
		        
		           while (res.next()) {
		               
		              
		               nom = res.getString("nom");
		               prenom = res.getString("prenom");
		               mail = res.getString("mail");
		               tel = res.getString("tel");
		               librairie = res.getString("librairie");
		               
		               
		           }
		           
		           user.setNom(nom);
		           user.setPrenom(prenom);
		           user.setMail(mail);
		           user.setTel(tel);
		           user.setLibrairie(librairie);
		           
		           
		           stm.close();
			       conn.close();
		           
		       } catch (ClassNotFoundException e) {
		           // TODO Auto-generated catch block
		           e.printStackTrace();
		       }
		        catch (SQLException e) {
		               // TODO Auto-generated catch block
		               e.printStackTrace();
		           }
		        
		        
				return user;
			
			}

	public void modifierUser(Users user, int id_user) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "update\r\n"
	           		+ "	       	users\r\n"
	           		+ "	       set\r\n"
	           		+ "	       	nom = '"+ user.getNom() +"',\r\n"
	           		+ "	       	prenom = '" + user.getPrenom() + "' ,\r\n"
	           		+ "	       	mail = '"+ user.getMail() +"' ,\r\n"
	           		+ "	       	tel = '"+ user.getTel() +"' ,\r\n"
	           		+ "	       	librairie = '"+ user.getLibrairie() +"' \r\n"
	           		+ "	       where\r\n"
	           		+ "	       	id_user ="+id_user+";";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
	}

	public void supprimerUser(int id_user) {
		// TODO Auto-generated method stub
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "delete from users where id_user = '"+id_user+"';";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
	}
	
	
	
}
