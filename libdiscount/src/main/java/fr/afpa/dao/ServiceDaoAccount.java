package fr.afpa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import fr.afpa.beans.Account;
import fr.afpa.beans.Users;

public class ServiceDaoAccount {

	public void creationAccountDao(Users users, int id_user) {
		
        
        
         Connection conn = null;
         Statement stm = null;
         
         try {
             
            Class.forName("org.postgresql.Driver");
            
            String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
            Properties props = new Properties();
            props.setProperty("user","libdiscount");
            props.setProperty("password","libdiscount");
            
            
            conn =  DriverManager.getConnection(url, props);
            
            System.out.println("Opened database successfully");
            
            
            stm = conn.createStatement();
            
            String query = "insert into account( id_account, login, mdp, id_user)"
                    + "values ( nextval('auto_inc_account') ,"
                    + "'" + users.getAccount().getLogin() + "',"
                    + "'" + users.getAccount().getMdp() + "',"
                    + "'" + id_user + "')";
            
            stm.executeUpdate(query);
            
          
            stm.close();
	        conn.close();
            
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
 
    }

	public int connexionAccount(String login, String mdp) {
		 Connection conn = null;
         Statement stm = null;
         int id = 0;
         
         try {
             
            Class.forName("org.postgresql.Driver");
            
            String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
            Properties props = new Properties();
            props.setProperty("user","libdiscount");
            props.setProperty("password","libdiscount");
            
            
            conn =  DriverManager.getConnection(url, props);
            
            System.out.println("Opened database successfully");
            
            
            stm = conn.createStatement();
            
            String query = "select id_user from account where login = '"+login+"' and mdp = '"+mdp+"';";
            
         
            
            ResultSet res = stm.executeQuery(query);
            
            
         
            while (res.next()) {
                
               
                id = res.getInt("id_user");
                
                
            }
            
            //int idServ = listeservice.getInt(1);
            
            stm.close();
	        conn.close();
            
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
         catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
         
         
        return id;
    }

	public Account recupAccount(int id_user) {
		// TODO Auto-generated method stub
		Account account = new Account();
		Connection conn = null;
        Statement stm = null;
        int id = 0;
        
        try {
            
           Class.forName("org.postgresql.Driver");
           
           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
           Properties props = new Properties();
           props.setProperty("user","libdiscount");
           props.setProperty("password","libdiscount");
           
           
           conn =  DriverManager.getConnection(url, props);
           
           System.out.println("Opened database successfully");
           
           
           stm = conn.createStatement();
           
           String query = "select * from account where id_user = '"+id_user+"';";
           
        
           
           ResultSet res = stm.executeQuery(query);
           
           String login = null;
           String mdp = null;
        
           while (res.next()) {
               
              
               login = res.getString("login");
               mdp = res.getString("mdp");
               
               account.setLogin(login);
               account.setMdp(mdp);
           }
           
           
           
           stm.close();
	        conn.close();
           
       } catch (ClassNotFoundException e) {
           // TODO Auto-generated catch block
           e.printStackTrace();
       }
        catch (SQLException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           }
        
        
		return account;
	}

	public void modifierAccount(Users user, int id_user) {
		Connection conn = null;
		Statement stm = null;
		try {
			
			 Class.forName("org.postgresql.Driver");
	           
	           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
	           Properties props = new Properties();
	           props.setProperty("user","libdiscount");
	           props.setProperty("password","libdiscount");
	           
	           
	           conn =  DriverManager.getConnection(url, props);
	          
	           System.out.println("Opened database successfully");
	  
	           stm = conn.createStatement();
	           String query = "update\r\n"
	           		+ "	       	account \r\n"
	           		+ "	       set\r\n"
	           		+ "	       	login = '"+ user.getAccount().getLogin() +"',\r\n"
	           		+ "	       	mdp = '" + user.getAccount().getMdp() + "' \r\n"
	           		+ "	       where\r\n"
	           		+ "	       	id_user ="+id_user+";";
	           

	           stm.executeUpdate(query);
	           
	           stm.close();
		       conn.close();
			

	        
		} catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		
	
		
	}

	public void supprimerAccount(int id_user) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Connection conn = null;
				Statement stm = null;
				try {
					
					 Class.forName("org.postgresql.Driver");
			           
			           String url = "jdbc:postgresql://localhost:5432/LibdiscountDB";
			           Properties props = new Properties();
			           props.setProperty("user","libdiscount");
			           props.setProperty("password","libdiscount");
			           
			           
			           conn =  DriverManager.getConnection(url, props);
			          
			           System.out.println("Opened database successfully");
			  
			           stm = conn.createStatement();
			           String query = "delete from account where id_user = '"+id_user+"';";
			           

			           stm.executeUpdate(query);
			           
			           stm.close();
				       conn.close();
					

			        
				} catch (Exception e) {
					// TODO: handle exception
					 e.printStackTrace();
				}
	}
		
	
		
	
	
}
