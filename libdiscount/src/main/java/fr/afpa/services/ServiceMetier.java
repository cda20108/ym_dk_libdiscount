package fr.afpa.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.beans.Account;
import fr.afpa.beans.Adresse;
import fr.afpa.beans.Annonce;
import fr.afpa.beans.Users;
import fr.afpa.dao.ServiceDaoAccount;
import fr.afpa.dao.ServiceDaoAdresse;
import fr.afpa.dao.ServiceDaoAnnonce;
import fr.afpa.dao.ServiceDaoUsers;

public class ServiceMetier {
	
	ServiceDaoUsers sdu = new ServiceDaoUsers();
	ServiceDaoAccount sdac = new ServiceDaoAccount();
	ServiceDaoAdresse sdad = new ServiceDaoAdresse();
	ServiceDaoAnnonce sdan = new ServiceDaoAnnonce();
	
	
	
	public void creationUsers() {
		Scanner in = new Scanner(System.in);
			
//________________ INFORMATIONS USERS _____________________
		System.out.println("Entrer votre nom : ");
		String nom = in.nextLine();
		System.out.println("Entrer votre prenom : ");
		String prenom = in.nextLine();
		System.out.println("Entrer votre mail : ");
		String mail = in.nextLine();
		System.out.println("Entrer votre numero de telephone : ");
		String tel = in.nextLine();
		System.out.println("Entrer le nom de votre librairie : ");
		String librairie = in.nextLine();
		
//________________ INFORMATIONS ADRESSE _____________________
		System.out.println("Entrer votre numero de rue : ");
		String numeroRue = in.nextLine();
		System.out.println("Entrer votre nom de rue : ");
		String nomRue = in.nextLine();
		System.out.println("Entrer votre code postal : ");
		String codePostal = in.nextLine();
		System.out.println("Entrer votre ville : ");
		String ville = in.nextLine();
		
		Adresse adresse = new Adresse(numeroRue, nomRue, codePostal, ville);
		

//________________ INFORMATIONS ACCOUNT _____________________
		System.out.println("Entrer votre login : ");
		String login = in.nextLine();
		System.out.println("Entrer votre mdp : ");
		String mdp = in.nextLine();
		
		Account account = new Account(login, mdp);
		
//________________ CREATION DE L'OBJET USERS _____________________
		
		Users users = new Users(nom, prenom, mail, tel, librairie, adresse, account);
		
		System.out.println(users);
		
		
//________________ APPEL DE LA FONCTION QUI REALISE L'AJOUT DANS LA BDD DE L'OBJET USERS _____________________		
		sdu.creationUserDao(users);
		int id_user = sdu.recuperationIdUser(nom, prenom);
		sdac.creationAccountDao(users, id_user);
		sdad.creationAdresseDao(users, id_user);
		
		
		
		
	}
	
	public int connexionUsers() {
		Scanner in = new Scanner(System.in);
		
		
		
//________________ INFORMATIONS ACCOUNT _____________________
		System.out.println("Entrer votre login : ");
		String login = in.nextLine();
		System.out.println("Entrer votre mdp : ");
		String mdp = in.nextLine();
		
		
		
		if (sdac.connexionAccount(login, mdp) != 0) {
			int user = sdac.connexionAccount(login, mdp);
			
			return user;
		}
		return -1;
	}
	
	public void creationAnnonce(int id_user) {
		
		
		Scanner in = new Scanner(System.in);
		
//________________ INFORMATIONS ANNONCE _____________________
		System.out.println("Entrer votre titre : ");
		String titre = in.nextLine();
		
		System.out.println("Entrer votre isbn : ");
		String isbn = in.nextLine();
		
		System.out.println("Entrer votre maison d'edition : ");
		String maisonEdition = in.nextLine();
		
		System.out.println("Entrer votre prix unitaire : ");
		Float prixUnitaire = in.nextFloat();
		in.nextLine();
		
		System.out.println("Entrer la remise en % : ");
		int remise = in.nextInt();
		in.nextLine();
		
		System.out.println("Entrer votre quantite : ");
		int quantite = in.nextInt();
		in.nextLine();
		
		
		Float prix_total = (prixUnitaire * quantite * remise) / 100 ;
		
		System.out.println(prix_total);
		
		
		System.out.println("Entrer l'ann�e d'edition : ");
		int ann�e = in.nextInt();
		
		System.out.println("Entrer le mois d'edition : ");
		int mois = in.nextInt();
		
		System.out.println("Entrer le jour d'edition : ");
		int jour = in.nextInt();
		
		LocalDate dateEdition = LocalDate.of(ann�e, mois, jour);
		
		LocalDate dateAnnonce = LocalDate.now();
		
		Annonce annonce = new Annonce(0 ,titre, isbn, maisonEdition, prixUnitaire, remise, quantite, prix_total, dateEdition, dateAnnonce);
		
		sdan.creationAnnonce(annonce, id_user);
		
		
		
	}
	
	
	public ArrayList<Annonce> listerAnnonce() {
		ArrayList<Annonce> listAnnonce = new ArrayList<Annonce>();
		
		listAnnonce = sdan.recupListeAnnonce();
		
		return listAnnonce;
	}
	
	public ArrayList<Annonce> listerAnnonceUser(int id_user) {
		ArrayList<Annonce> listAnnonce = new ArrayList<Annonce>();
		
		listAnnonce = sdan.recupListeAnnonceUsers(id_user);
		
		return listAnnonce;
	}
	
	public void suppressionAnnonce() {
		
	}
	
	/**
	 * 
	 * @param id_user
	 * @param id_annonce
	 */
	public void modificationAnnonce(int id_user, int id_annonce) {
		
		Annonce annonce = sdan.recupUneAnnonce(id_user, id_annonce);
		System.out.println(annonce);
		
		Scanner in = new Scanner(System.in);
		
		String menuConnexion = "Modifier : \n" + 
								"1 - Titre \n" +
								"2 - Isbn \n" +
								"3 - maison d'edition \n" +
								"4 - prix unitaire \n" +
								"5 - remise \n" +
								"6 - quantite \n" +
								"7 - date d'edition\n" +
								"8 - Quitter";
		
		System.out.println(menuConnexion);
		System.out.println("Choissez une action du menu :");
		String choixMenu = in.nextLine();
		while (!choixMenu.equals("8")) {
			
			switch (choixMenu) {
				case "1":
					System.out.println("Entrer le nouveau titre : ");
					String titre = in.nextLine();
					annonce.setTitre(titre);
					
					break;
				case "2":
					System.out.println("Entrer le nouveau ISBN : ");
					String isbn = in.nextLine();
					annonce.setIsbn(isbn);
					break;
				case "3":
					System.out.println("Entrer la nouvelle maison d'edition : ");
					String maisonEdition = in.nextLine();
					annonce.setMaisonEdition(maisonEdition);
					break;
				case "4":
					System.out.println("Entrer le nouveau prix unitaire : ");
					Float prixUnitaire= in.nextFloat();
					in.nextLine();
					annonce.setPrixUnitaire(prixUnitaire);
					break;
				case "5":
					System.out.println("Entrer la nouvelle remise : ");
					int remise = in.nextInt();
					in.nextLine();
					annonce.setRemise(remise);
					break;
				case "6":
					System.out.println("Entrer la nouvelle quantite : ");
					int quantite = in.nextInt();
					in.nextLine();
					annonce.setQuantite(quantite);
					break;
				case "7":
					System.out.println("Entrer la nouvelle date d'edition : ");
					String dateEdition = in.nextLine();
					LocalDate edition = LocalDate.parse(dateEdition);
					annonce.setDateEdition(edition);
					
					break;
				default:
					System.out.println("Veuilliez entrer un chiffre du menu !");
					break;
				}
			
			annonce.setPrix_total((annonce.getPrixUnitaire() * annonce.getQuantite() * annonce.getRemise()) / 100);
			
			System.out.println(annonce);
			System.out.println(menuConnexion);
			System.out.println("Choissez une action du menu :");
			choixMenu = in.nextLine();
		}
		System.out.println(annonce);
		sdan.modificationAnnonce(annonce, id_user, id_annonce);
		
	}

	
	/**
	 * 
	 * @param id_user
	 * @param id_annonce
	 */
	public void supprimerAnnonce(int id_user, int id_annonce) {
		
		sdan.supprimerAnnonceArchivage(id_user, id_annonce);
		
	}
	
	
	public Users recupCompte(int id_user) {
		
		Adresse adresse = sdad.recupAdresse(id_user);
		Account account = sdac.recupAccount(id_user);
		Users users = sdu.recupUser(id_user);
		
		users.setAdresse(adresse);
		users.setAccount(account);
		
		return users;
	}
	
	public void consulterCompte(int id_user) {
		
		Users user = recupCompte(id_user);
		String affichage = " - VOTRE COMPTE : _________ \n"
					+"Nom : "+ user.getNom()+"\n"
					+"Prenom : "+ user.getPrenom()+"\n"
					+"mail : "+ user.getMail()+"\n"
					+"tel : "+ user.getTel()+"\n"
					+"librairie : "+ user.getLibrairie()+"\n"
					+"Rue  : "+ user.getAdresse().getNumeroRue()+" "+user.getAdresse().getNomRue()+"\n"
					+"Code postal : "+ user.getAdresse().getCodePostal()+"\n"
					+"Ville : "+ user.getAdresse().getVille()+"\n"
					+"Login : "+ user.getAccount().getLogin()+"\n"
					+"Mot de passe : "+ user.getAccount().getMdp()+"\n";
		
		System.out.println(affichage);
		
		
		
	}

	public void modifierCompte(int id_user) {
		Scanner in = new Scanner(System.in);
		
		Users user = recupCompte(id_user);
		String affichage = " - VOTRE COMPTE : _________ \n"
				+"1 - Nom : "+ user.getNom()+"\n"
				+"2 - Prenom : "+ user.getPrenom()+"\n"
				+"3 - mail : "+ user.getMail()+"\n"
				+"4 - tel : "+ user.getTel()+"\n"
				+"5 - librairie : "+ user.getLibrairie()+"\n"
				+"6 - Rue : "+ user.getAdresse().getNumeroRue()+" "+user.getAdresse().getNomRue()+"\n"
				+"7 - Code postal : "+ user.getAdresse().getCodePostal()+"\n"
				+"8 - Ville : "+ user.getAdresse().getVille()+"\n"
				+"9 - Login : "+ user.getAccount().getLogin()+"\n"
				+"10 - Mot de passe : "+ user.getAccount().getMdp()+"\n"
				+"11 - Quitter";
	
	System.out.println(affichage);
	
	System.out.println("Choissez un champ � changer :");
	String choixMenu = in.nextLine();
	
	Adresse adresse = user.getAdresse();
	Account account = user.getAccount();
	
	while (!choixMenu.equals("11")) {
		switch (choixMenu) {
		case "1":
			System.out.println("Entrer le nouveau nom : ");
			String nom = in.nextLine();
			user.setNom(nom);
			break;
		case "2":
			System.out.println("Entrer le nouveau prenom : ");
			String prenom = in.nextLine();
			user.setPrenom(prenom);
			break;
		case "3":
			System.out.println("Entrer le nouveau mail : ");
			String mail = in.nextLine();
			user.setMail(mail);
			break;
		case "4":
			System.out.println("Entrer le nouveau numero de telephone : ");
			String tel = in.nextLine();
			user.setTel(tel);
			break;
		case "5":
			System.out.println("Entrer le nouveau librairie : ");
			String librairie = in.nextLine();
			user.setLibrairie(librairie);
			break;
		case "6":
			System.out.println("Entrer le nouveau numero de rue : ");
			String numeroRue = in.nextLine();
			adresse.setNumeroRue(numeroRue);
			user.setAdresse(adresse);
			System.out.println("Entrer le nouveau nom de rue : ");
			String nomRue = in.nextLine();
			adresse.setNomRue(nomRue);
			user.setAdresse(adresse);
			break;	
		case "7":
			System.out.println("Entrer le nouveau code postal : ");
			String codePostal = in.nextLine();
			adresse.setCodePostal(codePostal);
			user.setAdresse(adresse);
			break;
		case "8":
			System.out.println("Entrer la nouvelle ville : ");
			String ville = in.nextLine();
			adresse.setVille(ville);
			user.setAdresse(adresse);
			break;
		case "9":
			System.out.println("Entrer le nouveau numero de rue : ");
			String login = in.nextLine();
			account.setLogin(login);
			user.setAccount(account);
			break;
		case "10":
			System.out.println("Entrer le nouveau numero de rue : ");
			String mdp = in.nextLine();
			account.setMdp(mdp);
			user.setAccount(account);
			break;	
		default:
			break;
		}
		
		System.out.println(user);
		System.out.println(affichage);
		
		System.out.println("Choissez un champ � changer :");
		choixMenu = in.nextLine();
		
		sdu.modifierUser(user, id_user);
		sdad.modifierAdresse(user, id_user);
		sdac.modifierAccount(user, id_user);
		
		
	}
	
	
	}

	public void supprimerCompte(int id_user) {
		
		
		sdad.supprimerAdresse(id_user);
		sdac.supprimerAccount(id_user);
		sdan.supprimerAnnonce(id_user);
		sdu.supprimerUser(id_user);
		
		
	}
}
