package fr.afpa.view;

import java.util.ArrayList;
import java.util.Scanner;

import fr.afpa.beans.Annonce;
import fr.afpa.services.ServiceMetier;

public class Menu {
	
	ServiceMetier sm = new ServiceMetier();
	
	
	
	public void menuPrincipal() {
		Scanner in = new Scanner(System.in);
		String menuConnexion = "1 - Se connecter \n" + 
								"2 - S'incrire \n" +
								"3 - Quitter";
		
		
		System.out.println(menuConnexion);
		
		
		
		String menu;
		
		System.out.println("entrer un numero du menu : ");
		menu = in.nextLine();
		
		while (!menu.equals("3")) {
			
			switch (menu) {
			case "1":
				System.out.println("CONNEXION");
				int id_user = sm.connexionUsers();
				if (id_user !=-1) {
					
					interfaceClient(id_user);
				}
				
				
				break;
				
			case "2":
				System.out.println("INSCRIPTION");
				sm.creationUsers();
				
				break;

			default:
				System.out.println("Veuillez entrer un chiffre du menu !");
				break;
			}
			
			System.out.println(menuConnexion);
			System.out.println("entrer un numero du menu : ");
			menu = in.nextLine();
		}
		
		
		
		
		
	}
	
	public void interfaceClient(int id_user) {
		Scanner in = new Scanner(System.in);
		
		System.out.println("id du client connect� actuellement : "+ id_user);
		
		String menuClient = "1 - Consulter les annonces \n" + 
				"2 - Poster une annonce \n" +
				"3 - Voir ses annonce \n" +
				"4 - Supprimer une annonce \n" +
				"5 - Modifier une annonce \n" +
				"6 - Consulter son compte \n" +
				"7 - Modifier son compte \n" +
				"8 - Supprimer son compte \n" +
				"9 - Se deconnecter";
		
		
		
		String menu = "0";
		
		
		
		while (!menu.equals("9")) {
			
			
			System.out.println(menuClient);
			System.out.println("entrer un numero du menu : ");
			menu = in.nextLine();
			
			switch (menu) {
			case "1":
				System.out.println("CONSULTER LES ANNONCES");
				
				
				ArrayList<Annonce> listAnn =  sm.listerAnnonce();
				
				for (Annonce annonce : listAnn) {
					
					System.out.println(annonce);
				}
				break;
				
			case "2":
				System.out.println("POSTER");
				sm.creationAnnonce(id_user);
				
				break;
			case "3":
				System.out.println("VOIR SES ANNONCES");
				
				ArrayList<Annonce> listAnnUser =  sm.listerAnnonceUser(id_user);
				
				for (Annonce annonce : listAnnUser) {
					
					System.out.println(annonce);
				}
				
				break;	
				
			case "4":
				System.out.println("SUPPRIMER");
				ArrayList<Annonce> listAnnUserSup =  sm.listerAnnonceUser(id_user);
				
				for (Annonce annonce : listAnnUserSup) {
					
					System.out.println(annonce);
				}
				System.out.println("Choisir une annonce � modifier (numero ID_annonce) : ");
				int id_annonce = in.nextInt();
				in.nextLine();
				
				sm.supprimerAnnonce(id_user, id_annonce);
				
				break;
				
				
			case "5": // _______________ MODIFICATION _________________
				System.out.println("MODIFIER");
				ArrayList<Annonce> listAnnUserMod =  sm.listerAnnonceUser(id_user);
				
				for (Annonce annonce : listAnnUserMod) {
					
					System.out.println(annonce);
				}
				System.out.println("Choisir une annonce � modifier (numero ID_annonce) : ");
				int id_adresse = in.nextInt();
				in.nextLine();
				sm.modificationAnnonce( id_user, id_adresse);
				
				
				break;
			case "6": // _______________ CONSULTER COMPTE_________________
				System.out.println("CONSULTER COMPTE");
				sm.consulterCompte(id_user);
				break;
			case "7": // _______________ CONSULTER COMPTE_________________
				System.out.println("MODIFIER COMPTE");
				sm.modifierCompte(id_user);
				break;
			case "8": // _______________ CONSULTER COMPTE_________________
				System.out.println("SUPPRIMER COMPTE");
				System.out.println("�tes vous sur de vouloir supprimer votre compte ? ");
				System.out.println("1 - OUI - votre compte et vos annonces vont �tre supprim�es");
				System.out.println("2 - NON ");
				String verif = in.nextLine();
				if (verif.equals("1")) {
					System.out.println("Suppression du compte ..... 100%");
					System.out.println("Suppression des annonces .....100%");
					sm.supprimerCompte(id_user);
					menu = "9";
				}
				
				
				break;	

			default:
				System.out.println("Veuillez entrer un chiffre du menu !");
				break;
			}
			
			
		}
		
	}

	
}
