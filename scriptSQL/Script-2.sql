create table users( 
	id_user int primary key,
	Nom varchar(50) not null,
	Prenom varchar(50) not null,
	Mail varchar(50) not null,
	check (Mail like '%@%'),
	tel varchar(10) not null,
	librairie varchar(50) not null
);


create table account(
	id_account int primary key,
	login varchar(30) not null,
	mdp varchar(30) not null,
	id_user int references users(id_user)
);


create table adresse(
id_adresse varchar(50) primary key,
numero_rue varchar(5) not null,
nom_rue varchar(150) not null,
codepostal varchar(5) not null,
id_user int references users(id_user)
);

alter table adresse add ville varchar(50);


create table annonce(
id_annonce int primary key,
titre varchar(150) not null,
Isbn varchar(150) not null,
maison_edition varchar(150) not null,
prix_unitaire float not null,
remise int not null,
quantite int not null,
prix_total float not null,
date_edition varchar(50) not null,
date_annonce varchar(50) not null,
id_user int references users(id_user)
);


create table archive(
id_annonce int primary key,
titre varchar(150) not null,
ISBN varchar(150) not null,
maison_edition varchar(150) not null,
prix_unitaire float not null,
remise int not null,
quantite int not null,
prix_total float not null,
date_edition varchar(50) not null,
date_annonce varchar(50) not null,
id_user int not null
);




create sequence auto_inc_users
start with 1
increment by 1
minvalue 1
cycle;

create sequence auto_inc_adresse
start with 1
increment by 1
minvalue 1
cycle;

create sequence auto_inc_id_adresse
start with 1
increment by 1
minvalue 1
cycle;

create sequence auto_inc_account
start with 1
increment by 1
minvalue 1
cycle;

create sequence auto_inc_id_annonce
start with 1
increment by 1
minvalue 1
cycle;


-- FONCTION DE SUPPRESSION
create or replace function suppression()
returns trigger 
language plpgsql
as $$
declare 
	rec record;
begin
	for rec in (select * from annonce) loop
		if rec.id_annonce = old.id_annonce then
			insert into archive (id_annonce, titre, isbn, maison_edition, prix_unitaire, remise, quantite, prix_total, date_edition, date_annonce, id_user)
			values (old.id_annonce, old.titre, old.isbn, old.maison_edition, old.prix_unitaire, old.remise, old.quantite, old.prix_total, old.date_edition, old.date_annonce, old.id_user);
		end if;
	end loop;
return rec;
end;
$$;


-- CREATION DU TRIGGER POUR LES SUPPRESSIONS
create trigger archivage before
delete
	on
	annonce for each row execute function suppression();


-- AJOUT DE DONNEES 



insert into users( id_user, Nom, Prenom, Mail, tel, librairie)
values (nextval('auto_inc_users'),'Nacer','Berkat','nacerberkat@Mail.com','0123456789','Berkatlibrairie'),
	(nextval('auto_inc_users'),'Berrazzag','Imad','imadber@Mail.com','0123456789','BerrazzagBrairie'),
	(nextval('auto_inc_users'),'Bouchlaghem','Fouad','fouadBou@Mail.com','0123456789','Librairie fouad');



insert into adresse( id_adresse, numero_rue, nom_rue, codepostal, id_user, ville)
values (nextval('auto_inc_id_adresse'),'3','rue des hirondelles','62200',1, 'Lens'),
	(nextval('auto_inc_id_adresse'),'388','rue de larbrisseau','59000',2, 'Lille'),
	(nextval('auto_inc_id_adresse'),'86','rue de larbrisseau','59000',3, 'Lille');

insert into account( id_account, login, mdp, id_user)
values (nextval('auto_inc_account'),'berkat','berkat',1),
	(nextval('auto_inc_account'),'imad','imad',2),
	(nextval('auto_inc_account'),'fouad','fouad',3);

insert into annonce( id_annonce, titre, isbn, maison_edition, prix_unitaire, remise, quantite, prix_total, date_edition, date_annonce, id_user)
values (nextval('auto_inc_id_annonce'),'livre NACER','0132123132','BERKAT',10, 80, 20,500,'1999-08-25','1999-08-25', 1),
	(nextval('auto_inc_id_annonce'),'livre NACER 2','089754654','BERKAT',2, 60, 50,300,'1999-08-25','1999-08-25', 1),
	(nextval('auto_inc_id_annonce'),'Imad Livre','08464217','BERRAZZAG',18, 50, 200,4500,'1999-08-25','1999-08-25', 2),
	(nextval('auto_inc_id_annonce'),'LIVRE FOUAD','018794123','BOUCHLAGHEM',23, 70, 200,8410,'1999-08-25','1999-08-25', 3);


select * from users;
select * from adresse;
select * from annonce;
select * from account;
select * from archive;


